import { Component, OnInit } from '@angular/core';
import { check } from '../cuestionario-interface/cuestionario.interface';

@Component({
  selector: 'app-cuestionario',
  templateUrl: './cuestionario.component.html',
  styleUrls: ['./cuestionario.component.css']
})
export class CuestionarioComponent implements OnInit {

  cuestionario: check = {
    pregunta1: 0 ,
    pregunta2: 0 ,
    pregunta3: 0 ,
    pregunta4: 0 ,
    pregunta5: 0 ,
    aceptar: false
  }

  constructor() { }

  ngOnInit(): void {
  }

  Ir(): void {
    console.log(this.cuestionario.pregunta1);
    console.log(this.cuestionario.pregunta2);
    console.log(this.cuestionario.pregunta3);
    console.log(this.cuestionario.pregunta4);
    console.log(this.cuestionario.pregunta5);
    console.log(this.cuestionario.aceptar);
  }

}
